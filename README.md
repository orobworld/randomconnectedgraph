# Randomly Generating a Connected Graph

A [question](https://or.stackexchange.com/questions/10942/how-to-generate-random-connected-planar-graph) on Operations Research Stack Exchange asked about generating random test instances of a connected graph or digraph. The question specifies a [planar graph](https://en.wikipedia.org/wiki/Planar_graph), but I am pretty sure the author intended that to mean a two-dimensional graph as opposed to a graph with edges that do not cross. In any case, the code here does not enforce the no-crossing property.

The question presumes a given set of nodes divided into three groups (supply, transshipment and demand) with the stipulation that edges incident on supply nodes must also be incident on transshipment nodes (i.e., there are no edges connect supply nodes to demand nodes or other supply nodes). The goal is to randomly selected a specified number of edges such that the resulting graph is connected.

This repository contains a Java program that compares a mixed integer programming (MIP) model (solved with CPLEX) and an edge removal heuristic for generating connected graphs. The MIP model uses binary variables to choose edges and real-valued "flow" variables to enforce connectedness. Randomness is introduced by assigning random costs to the edges. The removal heuristic starts from a complete graph (other than the supply-supply and supply-demand edges that are prohibited) and randomly removes edges while preserving connectedness. Details can be found in my answer to the OR SE question and in a [blog post](https://orinanobworld.blogspot.com/2023/09/randomly-generating-connected-graph.html).

The program requires CPLEX (tested with version 22.1.1 but should work with pretty much any version), but is otherwise self-contained.
