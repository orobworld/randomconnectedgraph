package randomconnectedgraph;

/**
 * Edge represents an edge in the graph.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public record Edge(int tail, int head) { }
