package randomconnectedgraph;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * EdgeZapper attempts to build a suitable graph by starting with all possible
 * edges and randomly removing edges until either a connected graph of the
 * correct size is achieved or it proves impossible to continue.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class EdgeZapper {
  private final Graph graph;  // the target graph
  private final Random rng;   // the random number generator to use

  /**
   * Constructor.
   * @param g the complete graph from which to extract a subgraph
   */
  public EdgeZapper(final Graph g) {
    graph = g;
    rng = new Random();
  }

  /**
   * Attempts to build a suitable subgraph.
   * @return the set of indices of included edges, empty if the attempt fails
   */
  private Optional<Set<Integer>> buildGraph() {
    int target = graph.getTargetEdgeCount();
    // Clear any previous edge blockages.
    graph.unblockAll();
    // Get the set of all edge indices.
    Set<Integer> keep =
      IntStream.range(0, graph.getnEdges())
               .boxed().collect(Collectors.toSet());
    // Queue all edges for possible deletion, and shuffle the queue.
    LinkedList<Integer> queue = new LinkedList<>(keep);
    Collections.shuffle(queue, rng);
    // Try removing each edge in turn until either the target edge count is
    // reached or the queue is exhausted.
    while (!queue.isEmpty()) {
      // Get the index of the next edge to try dropping.
      int e = queue.pop();
      // Block the edge and confirm that the graph remains connected.
      Edge edge = graph.getEdge(e);
      graph.block(edge);
      if (graph.confirmPath(edge.tail(), edge.head())) {
        // The graph remains connected, so remove the edge from the solution.
        keep.remove(e);
        // Check whether the target has been hit.
        if (keep.size() == target) {
          return Optional.of(keep);
        }
      } else {
        // Removing the edge disconnects the graph, so unblock and retain it.
        graph.unblock(edge);
      }
    }
    // If we fall through to here, we have failed.
    return Optional.empty();
  }

  /**
   * Tries to build a subgraph via random trials.
   * @param nTrials the maximum number of trials
   * @param seed the seed for the random number generator
   * @return the set of indices of edges to include, empty if no success
   */
  public Optional<Set<Integer>> solve(final int nTrials, final long seed) {
    rng.setSeed(seed);
    for (int t = 0; t < nTrials; t++) {
      Optional<Set<Integer>> solution = buildGraph();
      if (solution.isPresent()) {
        System.out.println("Found solution after trial " + (t + 1) + ".");
        return solution;
      }
    }
    System.out.println("EdgeZapper failed.");
    return Optional.empty();
  }
}
