package randomconnectedgraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Graph holds the details of a problem instance.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Graph {
  private final int nSupply;            // number of supply nodes
  private final int nDemand;            // number of demand nodes
  private final int nTransit;           // number of transit nodes
  private final int edgeTarget;         // target number of edges to retain
  private final int nNodes;             // total node count
  private final ArrayList<Edge> edges;  // edges in the full graph
  // Map each node to indices of edges incident at it.
  private final HashMap<Integer, HashSet<Integer>> incidentAt;
  private final HashSet<Edge> unblocked;  // unblocked edges

  /**
   * Constructor.
   * @param nS the number of supply nodes
   * @param nD the number of demand nodes
   * @param nT the number of transshipment nodes
   * @param nE the target number of edges
   */
  public Graph(final int nS, final int nD, final int nT, final int nE) {
    nSupply = nS;
    nDemand = nD;
    nTransit = nT;
    nNodes = nSupply + nDemand + nTransit;
    edgeTarget = nE;
    // Build the list of all edges not connecting two supply nodes.
    edges = new ArrayList<>();
    for (int tail = 0; tail < nNodes; tail++) {
      for (int head = tail + 1; head < nNodes; head++) {
        if (head >= nSupply) {
          edges.add(new Edge(tail, head));
        }
      }
    }
    // Mark all edges as unblocked
    unblocked = new HashSet<>(edges);
    // Initialize the incidence map.
    incidentAt = new HashMap<>();
    for (int n = 0; n < nNodes; n++) {
      incidentAt.put(n, new HashSet<>());
    }
    // Build the incidence map.
    for (int e = 0; e < edges.size(); e++) {
      Edge edge = edges.get(e);
      incidentAt.get(edge.head()).add(e);
      incidentAt.get(edge.tail()).add(e);
    }
  }

  /**
   * Gets the number of supply nodes.
   * @return the number of supply nodes
   */
  public int getnSupply() {
    return nSupply;
  }

  /**
   * Gets the number of demand nodes.
   * @return the number of demand nodes
   */
  public int getnDemand() {
    return nDemand;
  }

  /**
   * Gets the number of transshipment nodes.
   * @return the number of transshipment nodes
   */
  public int getnTransit() {
    return nTransit;
  }

  /**
   * Gets the target number of edges.
   * @return the target number of edges
   */
  public int getTargetEdgeCount() {
    return edgeTarget;
  }

  /**
   * Gets the total number of nodes of all types.
   * @return the node count
   */
  public int getnNodes() {
    return nNodes;
  }

  /**
   * Gets the list of edges in the full graph.
   * @return the edge list
   */
  public ArrayList<Edge> getAllEdges() {
    return new ArrayList<>(edges);
  }

  /**
   * Gets the number of edges in the full graph.
   * @return the edge count
   */
  public int getnEdges() {
    return edges.size();
  }

  /**
   * Get the set of indices of edges incident at a node.
   * @param node the target node
   * @return the set of indices of incident edges
   */
  public Set<Integer> incidentEdges(final int node) {
    return new HashSet<>(incidentAt.get(node));
  }

  /**
   * Blocks an edge from consideration.
   * @param edge the edge to block
   */
  public void block(final Edge edge) {
    unblocked.remove(edge);
  }

  /**
   * Unblocks an edge.
   * @param edge the edge to unblock
   */
  public void unblock(final Edge edge) {
    unblocked.add(edge);
  }

  /**
   * Unblocks all edges.
   */
  public void unblockAll() {
    unblocked.addAll(edges);
  }

  /**
   * Finds the endpoint of an edge opposite a specified endpoint.
   * @param node the specified endpoint
   * @param edge the edge
   * @return the opposite endpoint
   */
  private int oppositeTo(final int node, final Edge edge) {
    if (edge.head() == node) {
      return edge.tail();
    } else if (edge.tail() == node) {
      return edge.head();
    } else {
      // The node is not an endpoint of the edge??
      throw new IllegalArgumentException("Edge " + edge
                                         + " is not incident at node "
                                         + node + "!");
    }
  }

  /**
   * Confirms the existence of a path between two nodes using only
   * unblocked edges.
   * @param node1 the index of the first node
   * @param node2 the index of the second node
   * @return true if a path is found
   */
  public boolean confirmPath(final int node1, final int node2) {
    LinkedList<Integer> todo = new LinkedList<>();
    HashSet<Integer> visited = new HashSet<>();
    todo.add(node1);
    // Process each node in the queue until either node2 is encountered
    // or the queue is exhausted.
    while (!todo.isEmpty()) {
      int node = todo.pop();
      visited.add(node);
      // Find unvisited adjacent nodes based solely on unblocked edges.
      for (int e : incidentAt.get(node)) {
        Edge edge = edges.get(e);
        if (unblocked.contains(edge)) {
          int n = oppositeTo(node, edge);
          // If this is the target endpoint, we have a path.
          if (n == node2) {
            return true;
          } else if (!visited.contains(n)) {
            // If the node has not been visited, mark it visited and queue it.
            visited.add(n);
            todo.add(n);
          }
        }
      }
    }
    // If we fall through to here, there is no path.
    return false;
  }

  /**
   * Confirms that the graph is connected using unblocked edges.
   * @return true if the subgraph is connected
   */
  public boolean confirmConnected() {
    HashSet<Integer> unvisited =  new HashSet<>();
    LinkedList<Integer> todo = new LinkedList<>();
    todo.add(0);
    for (int n = 0; n < nNodes; n++) {
      unvisited.add(n);
    }
    // Probe starting at node 0 until either all nodes are visited or the
    // graph is exhausted.
    while (!todo.isEmpty()) {
      int node = todo.pop();
      // Check all incident edges.
      for (int e : incidentAt.get(node)) {
        Edge edge = edges.get(e);
        if (unblocked.contains(edge)) {
          // If the edge is unblocked, find the opposite node.
          int n = oppositeTo(node, edge);
          // If the node is unvisited, mark it visited and add to the queue
          // unless already present.
          if (unvisited.contains(n)) {
            unvisited.remove(n);
            if (unvisited.isEmpty()) {
              // If all nodes have been visited, the graph is connected.
              return true;
            } else if (!todo.contains(n)) {
              todo.add(n);
            }
          }
        }
      }
    }
    // If we fall through to here, the graph is disconnected.
    return false;
  }

  /**
   * Validates a candidate solution.
   * If the solution is invalid, a message is printed.
   * @param solution the solution (as a set of indices of edges)
   * @return true if the solution is valid.
   */
  public boolean validateSolution(final Set<Integer> solution) {
    // Check the number of edges used.
    if (solution.size() != edgeTarget) {
      System.out.println("Number of edges in solution (" + solution.size()
                         + ") does not match target " + edgeTarget + ".");
      return false;
    }
    // Block all edges not in the solution.
    unblocked.clear();
    for (int s : solution) {
      unblocked.add(edges.get(s));
    }
    // Confirm the graph is connected.
    if (confirmConnected()) {
      return true;
    } else {
      System.out.println("The proposed graph is not connected.");
      return false;
    }
  }

  /**
   * Gets an edge based on its index.
   * @param index the index of the edge
   * @return the edge
   */
  public Edge getEdge(int index) {
    return edges.get(index);
  }
}
