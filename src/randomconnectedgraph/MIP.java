package randomconnectedgraph;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * MIP implements a MIP model to create a connected graph according to the
 * problem specifications.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP implements AutoCloseable {
  private static final double HALF = 0.5;  // used to round solutions
  private final IloCplex model;      // the MIP model
  private final IloNumVar[] use;     // indicators for inclusion of edges
  private final IloNumVar[] fFlow;   // flow variables in "forward" direction
  private final IloNumVar[] rFlow;   // flow variables in "reverse" direction

  /**
   * Constructor.
   * @param graph the full graph
   * @param seed seed for generating random edge costs
   * @throws IloException if CPLEX cannot build the model
   */
  public MIP(final Graph graph, final long seed) throws IloException {
    // Initialize the model.
    model = new IloCplex();
    int nEdges = graph.getnEdges();
    int nNodes = graph.getnNodes();
    // Create the edge use indicators.
    use = new IloNumVar[nEdges];
    for (int e = 0; e < nEdges; e++) {
      use[e] = model.boolVar("use_" + e);
    }
    // Create the flow variables, where for any edge (i, j) with index e,
    // fFlow[e] is the flow from i to j and rFlow[e] is the flow from j to i.
    fFlow = new IloNumVar[nEdges];
    rFlow = new IloNumVar[nEdges];
    for (int e = 0; e < nEdges; e++) {
      fFlow[e] = model.numVar(0, nNodes, "forward_flow_" + e);
      rFlow[e] = model.numVar(0, nNodes, "reverse_flow_" + e);
    }
    // Objective: minimize a randomly weighted sum of the edge use variables.
    Random rng = new Random(seed);
    double[] cost = rng.doubles().limit(nEdges).toArray();
    model.addMinimize(model.scalProd(cost, use));
    // Constraint: include exactly the target number of edges.
    model.addEq(model.sum(use), graph.getTargetEdgeCount(), "edge_target");
    // Constraint: for each node other than node 0, the flow into the node is
    // exactly 1 unit more than the flow out of the node.
    ArrayList<Edge> allEdges = graph.getAllEdges();
    for (int n = 1; n < nNodes; n++) {
      IloLinearNumExpr netFlow = model.linearNumExpr(); // flow in - flow out
      for (int e : graph.incidentEdges(n)) {
        Edge edge = allEdges.get(e);
        if (edge.head() == n) {
          // Forward flow enters the node.
          netFlow.addTerm(1.0, fFlow[e]);
          netFlow.addTerm(-1.0, rFlow[e]);
        } else {
          // Forward flow exits the node.
          netFlow.addTerm(-1.0, fFlow[e]);
          netFlow.addTerm(1.0, rFlow[e]);
        }
      }
      model.addEq(netFlow, 1.0, "net_flow_at_" + n);
    }
    // Constraint: there is no flow across an unused edge.
    for (int e = 0; e < nEdges; e++) {
      model.addLe(fFlow[e], model.prod(nNodes, use[e]), "forward_" + e);
      model.addLe(rFlow[e], model.prod(nNodes, use[e]), "reverse_" + e);
    }
  }

  /**
   * Solves the model.
   * @param timeLimit the time limit in seconds
   * @return the final solver status
   * @throws IloException if CPLEX blows up
   */
  public IloCplex.Status solve(final double timeLimit) throws IloException {
    model.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    model.solve();
    return model.getStatus();
  }

  /**
   * Gets the indices of the edges included in the solution.
   * @return the set of indices of edges used
   * @throws IloException if there is no solution or it cannot be recovered
   */
  public Set<Integer> getSolution() throws IloException {
    HashSet<Integer> solution = new HashSet<>();
    double[] vals = model.getValues(use);
    for (int e = 0; e < use.length; e++) {
      if (vals[e] > HALF) {
        solution.add(e);
      }
    }
    return solution;
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    model.close();
  }

}
