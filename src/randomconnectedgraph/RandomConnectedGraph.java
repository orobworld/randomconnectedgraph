package randomconnectedgraph;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import java.util.Optional;
import java.util.Set;

/**
 * RandomConnectedGraph demonstrates two approaches to generating randomly a
 * connected graph with a fixed number of edges and other restrictions.
 *
 * Problem source: https://or.stackexchange.com/questions/10942/how-to
 * -generate-random-connected-planar-graph/
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class RandomConnectedGraph {

  /** Dummy constructor. */
  private RandomConnectedGraph() { }

  /**
   * Creates a test instance and tries both approaches.
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Set the specifications for a test problem.
    int nSupply = 10;  // number of supply nodes
    int nDemand = 50;  // number of demand nodes
    int nTrans = 100;  // number of transshipment nodes
    int nEdges = 300;  // target number of edges
    Graph graph = new Graph(nSupply, nDemand, nTrans, nEdges);
    // Pick a random seed.
    long seed = 789;
    // Try the MIP model first, to ensure feasibility.
    System.out.println("Trying MIP model ...");
    IloCplex.Status status = IloCplex.Status.Infeasible;
    long time = System.currentTimeMillis();
    try (MIP mip = new MIP(graph, seed)) {
      status = mip.solve(120.);
      time = System.currentTimeMillis() - time;
      System.out.println("Final solver status = " + status);
      if (status == IloCplex.Status.Optimal
          || status == IloCplex.Status.Feasible) {
        // Get and validate the soluton.
        Set<Integer> solution = mip.getSolution();
        if (graph.validateSolution(solution)) {
          System.out.println("Solution is valid!");
        }
      }
      System.out.println("Elapsed time = " + time + " ms.");
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
    // If the problem is feasible, try the random edge zapping method.
    if (status == IloCplex.Status.Optimal
        || status == IloCplex.Status.Feasible) {
      System.out.println("\nTrying random constructor ...");
      time = System.currentTimeMillis();
      EdgeZapper zapper = new EdgeZapper(graph);
      int nTrials = 10;
      Optional<Set<Integer>> solution = zapper.solve(nTrials, seed);
      time = System.currentTimeMillis() - time;
      if (solution.isPresent()) {
        if (graph.validateSolution(solution.get())) {
          System.out.println("Zapper solution is valid!");
        }
      }
      System.out.println("Elapsed time = " + time + " ms.");
    }
  }

}
